# t-activity-options Specification

### Component Use

``` html

<t-activity-options
  resources="{{resources}}"
  id="{{id}}"
  details="{{activityDetails}}"
>
</t-activity-options>

```


### Options to Component

```javascript

	var resources = {
	    "buttons": {
	      "book": "Book Now",
	    }
	  }

```


#### Activity Options

```javascript

	var activityDetails = [
		  {
		  	"id": "XX12345"
		  },
		  {
		    "label": "Description",
		    "type": "text",
		    "content": "Explore desert botanical gardens, museums and walking trails at the Las Vegas Springs Preserve. The Springs Preserve offers visitors an interpretive look at Nevada's natural and cultural history."
		  },
		  {
		    "label": "Additional Info",
		    "type": "text",
		    "content": "Confirmation will be received at time of booking. Conditions of Sale: Once you have booked and received your travel voucher, the contact details for the travel service operator will be on this voucher under the heading 'Important Information'"
		  },
		  {
		    "label": "Policies",
		    "type": "text",
		    "content": "Once you have booked and received your travel voucher, the contact details for the travel service operator will be on this voucher under the heading 'Important Information'. Where applicable"
		  },
		  {
		    "label": "Photos",
		    "type": "photos",
		    "photos": [
		      "http://www.dynamic.viator.com/graphicslib/6473/SITours/springs-preserve-admission-in-las-vegas-181821.jpg",
		      "http://www.dynamic.viator.com/graphicslib/6473/SITours/springs-preserve-admission-in-las-vegas-181821.jpg",
		      "http://www.dynamic.viator.com/graphicslib/6473/SITours/springs-preserve-admission-in-las-vegas-181821.jpg"
		    ]
		  },
		  {
		    "label": "Schedule",
		    "type": "text",
		    "content": "Las Vegas, NevadaDailyOpen daily from 9am to 5pm (subject to change without notice; it is recommended that you call prior to your date of travel to confirm operating hours)Springs Preserve, Las VegasLas Vegas, Nevada, United States of America, North Americ."
		  },
		  {
		    "label": "Options",
		    "type": "options",
		    "content": [
		      {
		        "isAvailable": true,
		        "name": "1 Day Unlimited Pass",
		        "availableOn": [
		          {
		            "date": "Tue, 07 Feb, 2017",
		            "time": "12:00 AM"
		          },
		          {
		            "date": "Wed, 08 Feb, 2017",
		            "time": "12:00 AM"
		          },
		          {
		            "date": "Thu, 09 Feb, 2017",
		            "time": "12:00 AM"
		          }
		        ],
		        "passengerTypesInfo": [
		          {
		          	"isRefundable": false,
					"amount": 11.71,
					"currency": "USD",
		            "maxAge": 64,
		            "minAge": 18,
		            "minQuantity": 1,
		            "maxQuantity": 9,
		            "passengerType": "Adult"
		          },
		          {		            
		          	"isRefundable": false,
					"amount": 8.71,
					"currency": "USD",
		            "maxAge": 12,
		            "minAge": 2,
		            "minQuantity": 1,
		            "maxQuantity": 9,
		            "passengerType": "Child"
		          }
		        ]
		      },
		      {
		        "isAvailable": true,
		        "name": "Half Day Limited Pass",
		        "availableOn": [
		          {
		            "date": "Tue, 07 Feb, 2017",
		            "time": "12:00 AM"
		          },
		          {
		            "date": "Wed, 08 Feb, 2017",
		            "time": "12:00 AM"
		          },
		          {
		            "date": "Thu, 09 Feb, 2017",
		            "time": "12:00 AM"
		          }
		        ],
		        "passengerTypesInfo": [
		          {		            
		          	"isRefundable": false,
					"amount": 8.71,
					"currency": "USD",
		            "maxAge": 64,
		            "minAge": 18,
		            "minQuantity": 1,
		            "maxQuantity": 9,
		            "passengerType": "Adult"
		          },
		          {		            
		          	"isRefundable": false,
					"amount": 6.71,
					"currency": "USD",
		            "maxAge": 12,
		            "minAge": 2,
		            "minQuantity": 1,
		            "maxQuantity": 9,
		            "passengerType": "Child"
		          }
		        ]
		      }
		    ]
		  }
		]
```


## Important Information

- Misc items to be handled from CSS like:
1. List card background color
2. Mixin (to control text color, font etc)
3. Mixin (Button color, size etc.)


## Test Cases
- Basic validation for filter

## Steps to Start
- Set Github repository at your end for this project, we will merge them later
- APIs Integration - Use Tavisca's APIs for integration purpose.

## Performance standard
- Any component if opened via [web page tester](https://www.webpagetest.org/), it should load under 500ms (milli seconds).

## Documents
- Visual designs for search components - https://projects.invisionapp.com/share/6E9PJ7R4Q#/screens/212067485
- API access : Url - http://demo.travelnxt.com/dev
- Tavisca Elements - https://github.com/atomelements and https://github.com/travelnxtelements
- Vaadin elements - https://vaadin.com/docs/-/part/elements/elements-getting-started.html
- Google - https://elements.polymer-project.org/browse?package=google-web-components
- Tavisca Web component style Guide - https://drive.google.com/open?id=0B7BT_2nBFNYVR2tscE9pRnVJYmc

## Ballpark Estimates

30 Days / 6 Weeks
- Estimates are including above requrement with responsive design.
- Estimates are for single resourse.
- These are ballpark estimates and can be more/less while actual development.
